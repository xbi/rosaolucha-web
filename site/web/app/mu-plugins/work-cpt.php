<?php
/**

* Plugin Name: Work Custom Post Type
* Plugin URI: https://rosaolucha.com
* Description: Work post type for the Rosaolucha.com site
* Version: 1.0.1
* Author: Xavier Bonell
* Author URI: https://xbonell.com
* License: GPL2

*/

// Register Work Post Type
// ----------------------------------------------------------------------------
function work_post_type() {

  $labels = array(
    'name'                  => _x( 'Works', 'Post Type General Name', 'rosaolucha' ),
    'singular_name'         => _x( 'Work', 'Post Type Singular Name', 'rosaolucha' ),
    'menu_name'             => __( 'Works', 'rosaolucha' ),
    'name_admin_bar'        => __( 'Work', 'rosaolucha' ),
    'archives'              => __( 'Work Archives', 'rosaolucha' ),
    'attributes'            => __( 'Work Attributes', 'rosaolucha' ),
    'parent_item_colon'     => __( 'Parent Work:', 'rosaolucha' ),
    'all_items'             => __( 'All Works', 'rosaolucha' ),
    'add_new_item'          => __( 'Add New Work', 'rosaolucha' ),
    'add_new'               => __( 'Add New', 'rosaolucha' ),
    'new_item'              => __( 'New Work', 'rosaolucha' ),
    'edit_item'             => __( 'Edit Work', 'rosaolucha' ),
    'update_item'           => __( 'Update Work', 'rosaolucha' ),
    'view_item'             => __( 'View Work', 'rosaolucha' ),
    'view_items'            => __( 'View Works', 'rosaolucha' ),
    'search_items'          => __( 'Search Works', 'rosaolucha' ),
    'not_found'             => __( 'Not found', 'rosaolucha' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'rosaolucha' ),
    'featured_image'        => __( 'Featured Image', 'rosaolucha' ),
    'set_featured_image'    => __( 'Set featured image', 'rosaolucha' ),
    'remove_featured_image' => __( 'Remove featured image', 'rosaolucha' ),
    'use_featured_image'    => __( 'Use as featured image', 'rosaolucha' ),
    'insert_into_item'      => __( 'Insert into item', 'rosaolucha' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'rosaolucha' ),
    'items_list'            => __( 'Works list', 'rosaolucha' ),
    'items_list_navigation' => __( 'Works list navigation', 'rosaolucha' ),
    'filter_items_list'     => __( 'Filter works list', 'rosaolucha' ),
  );

  $args = array(
    'label'                 => __( 'Work', 'rosaolucha' ),
    'description'           => __( 'Work post type for Rosaolucha.com', 'rosaolucha' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'revisions', 'page-attributes', 'thumbnail'),
    'taxonomies'            => array( 'category' ),
    'hierarchical'          => true,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 20,
    'menu_icon'             => 'dashicons-schedule',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );

  register_post_type( 'work', $args );

}

add_action( 'init', 'work_post_type', 0 );