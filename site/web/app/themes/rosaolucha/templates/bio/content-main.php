<?php 
  use Roots\Sage\Helpers;
?>
<?php if ( have_rows('endorsements') ) : ?>
<div class="endorsements l-row">
  
  <div class="l-inner">
    
    <?php while ( have_rows('endorsements') ) : the_row(); ?>

      <!-- name, description, text, picture -->
      <div class="endorsement">
        <div class="endorsement__inside">
          <div class="endorsement__picture">
            <?php if ( get_sub_field('picture') ): echo Helpers\get_image('picture'); endif; ?>
            <svg class="icon icon-arrow-up"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-up"></use></svg>
          </div>
          <div class="endorsement__content">
            <span class="name"><?php the_sub_field('name'); ?></span>
            <span class="description"><?php the_sub_field('description'); ?></span>
          </div>
        </div>
        <div class="endorsement__over">
          <div class="inner"><?php echo strip_tags(get_sub_field('text'), '<p><br><i><em><strong><b>'); ?></div>
        </div>
      </div>


    <?php endwhile; ?>

  </div><!-- /.l-inner -->

</div><!-- /.endorsements -->
<?php endif; ?>