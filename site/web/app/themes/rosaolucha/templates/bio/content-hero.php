<?php 
  use Roots\Sage\Helpers;
?>
<div class="bio-content">
  
  <div class="l-row">
 
    <div class="l-left">
      <div class="image-box bio-content__foto--1"><?= Helpers\get_image('foto_1'); ?></div>
    </div><!-- /.l-left -->
 
    <div class="l-right">
      <div class="text-box bio-content__text--1"><?php the_field('text_1'); ?></div>
    </div><!-- /.l-right -->
 
  </div><!-- /.l-row -->
  
  <div class="l-row">

    <div class="l-left">

      <div class="bio-content__allworks">
        <div class="text-box bio-content__text--2"><?php the_field('texto_2'); ?></div>
        <div><a href="/all-work/" class="btn btn--inverted"><?php _e( 'All works', 'rosaolucha'); ?></a></div>
      </div>
      
      <div class="image-box image-box--overflow--left  bio-content__foto--2"><?= Helpers\get_image('foto_2'); ?></div>
    
    </div><!-- /.l-left -->
  
    <div class="l-right">
      <div class="bio-content__minigallery">
        <div class="image-box bio-content__foto--3"><?= Helpers\get_image('foto_3'); ?></div>
        <div class="image-box bio-content__foto--4"><?= Helpers\get_image('foto_4'); ?></div>
      </div>
      <div class="text-box bio-content__text--3"><?php the_field('texto_3'); ?></div>
    </div><!-- /.l-right -->
  
  </div><!-- /.l-row -->

</div>
<!-- /.bio -->