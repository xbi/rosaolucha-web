<?php if ( have_rows('homepage_endorsements') ) : ?>

<section class="home-endorsements l-row">

    <div class="l-inner">

      <div class="home-endorsements__header">
        <p class="home-endorsements__title"><?php the_field('homepage_endorsements_title'); ?></p>
        <p class="home-endorsements__link"><a href="/bio/">Bio</a></p>
      </div><!-- /.home-endorsements__header -->    

      <div class="flexslider endorsements-slider home-endorsements__slider">
        <ul class="slides">
          <?php while ( have_rows('homepage_endorsements') ) : the_row(); ?>
          <li>
            <div>
              <span class="title"><?php the_sub_field('homepage_endorsement_title'); ?></span>
              <span class="text"><?php the_sub_field('homepage_endorsement_text'); ?></span>
            </div>
          </li>
          <?php endwhile; ?>
        </ul>
      </div><!-- /.endorsements-slider -->    

    </div><!-- /.l-inner -->

</section>
<!-- /.home-endorsements -->

<?php endif; ?>