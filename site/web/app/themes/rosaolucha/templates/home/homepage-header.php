<?php
  use Roots\Sage\Helpers;
?>
<section class="home-header l-row">

    <div class="l-inner">
      <h1 class="home-header__brand">
        <a href="/"><svg class="logo"><use xlink:href="#logo-full"></use></svg></a>
      </h1>
      <div class="home-header__video">
        <div class="video" data-wrapper=".home-header">
          <div class="video_wrap">
            <?= Helpers\embed_video(get_field('video_homepage')); ?>
            <svg class="icon icon-play" aria-hidden="true"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#play"></use></svg>
          </div>
        </div>
      </div>
    </div><!-- /.l-inner -->

</section>
<!-- /.home-header -->

