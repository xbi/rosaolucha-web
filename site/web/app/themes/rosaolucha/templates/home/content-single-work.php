<?php
  use Roots\Sage\Helpers;
?>
<div class="work-card work-card--home <?= get_post_field( 'post_name', $post->ID ); ?> <?php if ( !empty( get_field('color_theme') ) ): echo 'theme-' . get_field('color_theme'); endif; ?>">
  
  <div class="picture"><?php the_post_thumbnail(); ?></div><!-- /.video -->

  <div class="content">
    
    <div class="content__header">
      <h2 class="title"><?php the_title(); ?></h2>
      <p class="subtitle"><?php the_field('tagline_2'); ?></p>
      <a href="<?php echo get_the_permalink(); ?>" class="btn btn--sm btn--inverted"><?php _e('Más detalles','rosaolucha'); ?></a>
    </div>

    <div class="content__footer">
      <div class="logo"><?= Helpers\get_image('channel_logo'); ?></div>
      <?php if ( get_field('channel_logo_2') ): ?><div class="logo"><?= Helpers\get_image('channel_logo_2'); ?></div><?php endif; ?>
    </div>

  </div><!-- /.content -->

  <div class="tagline"><?php the_field('tagline_3'); ?>.</div>

</div><!-- /.work-card -->