<?php

  use Roots\Sage\Helpers;

  /*
  $args = array(
    'post_type' => 'work',
    'posts_per_page' => -1,
    'post_parent' => 0,
    'order' => 'ASC',
    'orderby' => 'title',
    'meta_key' => 'selected_work',
    'meta_value' => true
  );
  */

  if ( have_rows('selected_works') ) :

?>

<section class="home-works l-row">

    <div class="l-inner">

      <div class="home-works__header">
        <h2 class="home-works__title">
          <svg class="icon icon-arrow-down" aria-hidden="true"><use xlink:href="#arrow-down"></use></svg>
          <span><?php _e('Selected work','rosaolucha'); ?></span>
        </h2>
        <p class="home-works__link"><a href="/all-work/"><?php _e('All work','rosaolucha'); ?></a></p>
      </div><!-- /.home-works__header -->

      <?php while( have_rows('selected_works') ) : the_row();
        // $distribuidora = $post->post_name;
        $post = get_sub_field('selected_work');
        setup_postdata($post);
        get_template_part('templates/home/content-single', 'work');
        wp_reset_postdata();
      endwhile; ?>

      <div class="home-works__footer">
        <a href="/all-work/" class="btn">All work</a>
      </div><!-- /.home-works__footer -->

    </div><!-- /.l-inner -->

</section>
<!-- /.home-works -->

<?php endif; ?>