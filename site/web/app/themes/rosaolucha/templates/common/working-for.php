<?php 
  
  use Roots\Sage\Helpers;

  $homepageID = get_option( 'page_on_front' );

  if ( have_rows( 'workingfor_logos', $homepageID ) ) :

?>
<section class="working-for l-row">

    <div class="l-inner">

      <?php if ( !empty( get_field('workingfor_title',$homepageID) ) ) : ?>
      <p class="working-for__title"><?php the_field( 'workingfor_title', $homepageID ); ?></p>
      <?php endif; ?>      

      <ul class="working-for__logos">
        <?php while ( have_rows( 'workingfor_logos', $homepageID ) ) : the_row(); ?>
          <li class="working-for__logo">
            <?= Helpers\get_image('workingfor_logo'); ?>
          </li><!-- /.working-for__logo -->
        <?php endwhile; ?>
      </ul><!-- /.working-for__logos -->

    </div><!-- /.l-inner -->

</section>
<!-- /.working-for -->
<?php endif; wp_reset_postdata(); ?>