<?php
  use Roots\Sage\Breadcrumbs;
  use Roots\Sage\Titles;
  use Roots\Sage\Works;
?>
<?php while (have_posts()) : the_post(); ?>

  <article <?php post_class(); ?>>

    <?php get_template_part('templates/page', 'header'); ?>

    <div class="work-content">
      
      <?php
      
        if ( have_rows('contents') )                                     get_template_part('templates/work/content', 'main');
        if ( have_rows('videohighlights') )                              get_template_part('templates/work/content', 'videohighlights');
        if ( have_rows('highlights') )                                   get_template_part('templates/work/content', 'highlights');
        if ( get_field('ratings_text') || get_field('ratings_table_1') ) get_template_part('templates/work/content', 'ratings');
        if ( have_rows('reviews_images') || have_rows('reviews') )       get_template_part('templates/work/content', 'reviews');
        if ( have_rows('awards') )                                       get_template_part('templates/work/content', 'awards');

      ?>

    </div>
  
    <?php
      $parent = $post->post_parent;
      if ( $post->post_parent ) :
    ?>
    <footer class="work-footer l-row">
      <div class="l-inner">
        <span class="work-footer__parent"><?php echo get_the_title($parent); ?></span>
        <nav class="work-footer__menu"><?= Works\nav(); ?></nav>
      </div>
    </footer>
    <?php endif; ?>
  
  </article>

<?php endwhile; ?>
