<?php
  use Roots\Sage\Helpers;
?>
<section class="work-highlights work-content__section l-row">
  <div class="l-inner">
    
    <?php

    if ( have_rows('highlights') ) :
      
      echo '<div class="flexslider highlights-slider work-highlights__slider">';
      echo '  <ul class="slides">';

      while ( have_rows('highlights') ) : the_row();
        echo '<li>';
        echo '  <div>';
        echo '    <span class="title">' . get_sub_field('highlight_title') . '</span>';
        echo '    <span class="subtitle">' . get_sub_field('highlight_text') . '</span>';
        echo '  </div>';
        echo '</li>';
      endwhile;

      echo '  </ul><!-- /.slides -->';
      echo '</div><!-- /.flexslider -->';

    endif;

    if ( get_field('highlights_logo') ) :

    ?>

    <div class="work-highlights__logo">
    <?= Helpers\get_image('highlights_logo'); ?>
    </div><!-- /.work-highlights__logo -->

    <?php endif; ?>
  
  </div><!-- /.l-inner -->

</section><!-- /.work-highlights -->