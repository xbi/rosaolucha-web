<section class="work-content__section work-main l-row">
  <div class="l-inner">

  <?php

    if ( have_rows('contents') ) :

      $sectionContent = array( 'left' => '', 'right' => '');

      while ( have_rows('contents') ) : the_row();

        $content_type = get_sub_field('content_type');
        $content_position = get_sub_field('content_position');

        ob_start();
        get_template_part('templates/work/content-main', $content_type);
        $sectionContent[$content_position] .= ob_get_clean();

      endwhile;

      foreach ($sectionContent as $position => $content) {
        echo '<div class="l-' . $position . '">' . $content . '</div><!-- /.l-' . $position . ' -->';
      }

    endif;

  ?>

  </div><!-- /.l-inner -->
</section><!-- /.work-main -->