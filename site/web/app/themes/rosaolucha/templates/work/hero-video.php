<?php
  use Roots\Sage\Helpers;
?>
<div class="hero-video">
  
  <div class="video hero-video__player">
    <div class="video__wrap">
      <?= Helpers\embed_video( get_field('video_url') ); ?>
      <svg class="icon icon-play" aria-hidden="true"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#play"></use></svg>
    </div>
    <div class="hero-video__deco" aria-hidden="true"><?= Helpers\get_image('video_cover'); ?></div>
  </div><!-- /.hero-video__player -->

  <div class="hero-video__caption">
    <h2 class="hero-video__title"><?php the_field('tagline_1'); ?></h2>
    <p class="hero-video__subtitle"><?php the_field('tagline_2'); ?></p>
  </div><!-- /.hero-video__caption -->

  <?php

    if ($post->post_parent) {

      echo '<nav class="hero-video__nav">';
      echo '  <ul>';

      wp_list_pages(array(
          'post_type'   => 'work',
          'child_of'    => $post->post_parent,
          'depth'       => 1,
          'title_li'    => '',
          'sort_column' => 'title'
      ));

      echo '  </ul>';
      echo '</nav> <!-- /.hero-video__nav -->';

    }

  ?>

</div><!-- /.hero-video -->