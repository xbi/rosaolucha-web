<?php
  use Roots\Sage\Helpers;
?>
<div class="image-box image-box--<?php the_sub_field('content_image_style'); ?>">
  <?= Helpers\get_image('content_image'); ?>
</div><!--/.image-box -->