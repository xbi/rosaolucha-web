<section class="work-ratings work-content__section l-row">
  <div class="l-inner">
    
    <?php if ( get_field('ratings_text') ) : ?>
    <div class="l-top">
      <div class="text-box">
        <?php the_field('ratings_text'); ?>
      </div><!-- /.text-box -->
    </div><!-- /.l-top -->
    <?php endif; ?>
  
    <?php if ( get_field('ratings_table_1') ) : ?>
    <div class="l-left">
      <?php the_field('ratings_table_1'); ?>
    </div><!-- /.l-left -->
    <?php endif; ?>

    <?php if ( get_field('ratings_table_2') ) : ?>
    <div class="l-right">
      <?php the_field('ratings_table_2'); ?>
    </div><!-- /.l-right -->
    <?php endif; ?>

  </div><!-- /.l-inner -->

</section><!-- /.work-ratings -->