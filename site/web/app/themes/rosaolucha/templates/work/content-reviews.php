<section class="work-content__section work-reviews l-row">
  <div class="l-inner">
    
    <?php

    if ( have_rows('reviews_images') || have_rows('reviews') ):
      
      $sectionContent = array( 'left' => '', 'right' => '', 'bottom' => '' );  
      $sliderContent  = '';

      $sliderContent .= '<div class="flexslider reviews-slider">';
      $sliderContent .= '  <div class="reviews-slider__header">';
      $sliderContent .= '    <p class="title">' . get_field('reviews_section_title') . '</p>';
      $sliderContent .= '    <p class="subtitle">' . get_field('reviews_title') . '</p>';      
      $sliderContent .= '  </div><!-- /.reviews-slider__header -->';
      $sliderContent .= ' <ul class="slides">';

      while ( have_rows('reviews') ) : the_row();
        ob_start();
        get_template_part('templates/work/content-reviews', 'slide');
        $sliderContent .= ob_get_clean();        
      endwhile;

      $sliderContent .= '  </ul>';
      $sliderContent .= '</div><!-- .flexslider -->';
      $sectionContent['right'] .= $sliderContent;

      
      while ( have_rows('reviews_images') ) : the_row();
    
        $content_type = 'image';
        $content_position = get_sub_field('reviews_image_position');
        
        ob_start();
        get_template_part('templates/work/content-reviews', $content_type);
        $sectionContent[$content_position] .= ob_get_clean();

      endwhile;

      foreach ($sectionContent as $position => $content) {
        echo '<div class="l-' . $position . '">' . $content . '</div><!-- /.l-' . $position . ' -->';
      }      

    endif;

    $review_images = get_field('reviews_images');

    if ( !empty($review_images) ) {
      $first_img = $review_images[0]['reviews_image']['url'];
      echo '<style>@media (max-width: 767px) { .work-reviews .l-right { background-image: url("' . $first_img . '"); } }</style>';
    }

    ?>
  
  </div><!-- /.l-inner -->

</section><!-- /.work-reviews -->