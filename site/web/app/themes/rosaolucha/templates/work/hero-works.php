<?php

  $distribuidora = $post->post_name;
  $order = ('alaska-y-mario' === $distribuidora) ? 'ASC' : 'DESC';

  $args = array(
    'post_type' => 'work',
    'posts_per_page' => -1,
    'post_parent' => $post->ID,
    'order' => $order,
    'orderby' => 'menu_order'
  );

  $parent = new WP_Query( $args );


  if ( $parent->have_posts() ) :

?>

<div class="hero-works">

<?php while( $parent->have_posts() ) : $parent->the_post();
  $titleArr = explode(" ", get_the_title());
  $season = $titleArr[0];
  $number = $titleArr[1];
  include(locate_template('templates/distribuidora/content-' . $distribuidora . '.php'));
endwhile; ?>

</div><!-- /.hero-works -->

<?php endif; wp_reset_query(); ?>
