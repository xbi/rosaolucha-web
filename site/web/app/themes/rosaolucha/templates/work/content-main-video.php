<?php
  use Roots\Sage\Helpers;
?>
<div class="video video-promo">
  <a href="<?php the_sub_field('content_video_url'); ?>" data-lity>
    <?= Helpers\get_image('content_video_cover'); ?>
    <svg class="icon icon-play" aria-hidden="true"><use xlink:href="#play"></use></svg>
    <span><?php _e('video promo','rosaalucha'); ?></span>
  </a>  
</div><!--/.video-promo -->