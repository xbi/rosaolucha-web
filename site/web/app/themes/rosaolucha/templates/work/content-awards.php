<?php
  use Roots\Sage\Helpers;
?>
<section class="work-content__section work-awards l-row">
  <div class="l-inner">
    
    <?php if ( have_rows('awards') ) : ?>

    <div class="awards">
  
      <h2 class="awards__title"><?php _e('Premios', 'rosaolucha'); ?></h2>

      <div class="awards__content">

      <?php while ( have_rows('awards') ) : the_row(); ?>
      
        <div class="award-item">
          <?php if ( get_sub_field('award_image') ): ?>
          <span class="award-item__image"><?= Helpers\get_image('award_image'); ?></span>
          <?php else: ?>
          <span class="award-item__icon"><svg class="icon icon-award" aria-hidden="true"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#award"></use></svg></span>  
          <?php endif; ?>
          <h3 class="award-item__title"><?php the_sub_field('award_title'); ?></h3>
          <p class="award-item__text"><?php the_sub_field('award_text'); ?></p>
        </div><!-- /.awards-item -->

      <?php endwhile; ?>

      </div><!-- /.awards__content -->

    </div><!-- /.awards -->
  
    <?php endif; ?>
  
  </div><!-- /.l-inner -->

</section><!-- /.work-reviews -->