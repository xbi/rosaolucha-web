<?php
  use Roots\Sage\Helpers;
?>
<li>
  <div>
    <span class="text"><?php the_sub_field('review_text'); ?></span>
    <?php if ( get_sub_field('review_pdf') ) :?><span class="link"><a href="<?php echo get_sub_field('review_pdf')['url']; ?>" data-lity><?php _e('Pdf','rosaolucha'); ?></a></span><?php endif; ?>
    <?= Helpers\get_image('review_logo'); ?>
  </div>
</li>