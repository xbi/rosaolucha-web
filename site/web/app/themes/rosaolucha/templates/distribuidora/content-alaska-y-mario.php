<?php
  use Roots\Sage\Helpers;
?>
<a href="<?php echo get_the_permalink(); ?>" class="work-card work-card--alaska-y-mario">
  <?php if ( has_post_thumbnail() ) : the_post_thumbnail(); else: echo Helpers\get_image('video_cover'); endif; ?>
  <div class="content">
    <h2 class="content__title">
      <?php echo $season; ?>
      <?php if ( !empty($number) ) : ?><span><?php echo $number; ?></span><?php endif; ?>
    </h2><!-- /.content__title -->
    <div class="content__meta">
      <span class="description"><?php the_field('tagline_1'); ?></span>
      <span class="tagline"><?php the_field('tagline_2'); ?></span>
    </div><!-- /.content__meta -->
    <span class="content__year"><?php the_field('year'); ?></span>
  </div><!-- /.content -->
  <div class="over">
    <span class="btn btn--sm btn--inverted"><?php _e('Más detalles','rosaolucha'); ?></span>
  </div><!-- /.over -->
</a><!-- /.work-card -->