<?php
  use Roots\Sage\Helpers;
?>
<div class="work-card work-card--galas">
  
  <div class="video">
    <div class="video__wrap">
      <?= Helpers\embed_video( get_field('video_url') ); ?>
      <svg class="icon icon-play" aria-hidden="true"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#play"></use></svg>
    </div>
  </div><!-- /.video -->

  <div class="content">
    
    <div class="content__header">
      <h2 class="title"><?php the_title(); ?></h2>
      <p class="subtitle"><?php _e('Rosa Olucha','rosaolucha'); ?>. <?php the_field('description'); ?>.</p>
      <a href="<?php echo get_the_permalink(); ?>" class="btn btn--sm btn--inverted"><?php _e('Más detalles','rosaolucha'); ?></a>
    </div>

    <div class="content__footer">
      <div class="logo"><?= Helpers\get_image('channel_logo'); ?></div>
    </div>

  </div><!-- /.content -->

  <div class="tagline"><?php the_field('tagline_1'); ?>.</div>

</div><!-- /.work-card -->