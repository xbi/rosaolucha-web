<header class="banner" role="banner">
  <div class="container">
    <a class="brand" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>"><svg class="logo logo--compact" aria-hidden="true"><use xlink:href="#logo-compact"></use></svg></a>
    <div class="nav-wrap">
      <nav class="nav-primary" role="navigation">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
        endif;
        ?>
        <?php
        if (has_nav_menu('follow_menu_mobile')) :
          echo '<div class="nav-primary__follow nav-contact">';
          echo '  <p class="nav-contact__title">' . __('Follow','rosaolucha') . '</p>';
          wp_nav_menu([
            'theme_location' => 'follow_menu_mobile',
            'menu_class' => 'nav-contact__menu',
            'walker' => new social_icons_walker()
          ]);
          echo '</div>';
        endif;
        ?>      
      </nav>
    </div>
  </div>
</header>
<!-- /.banner -->

<button type="button" data-toggle="menu">
  <span></span>
  <span></span>
  <span></span>
</button>
<!-- /[data-toggle="menu"] -->
