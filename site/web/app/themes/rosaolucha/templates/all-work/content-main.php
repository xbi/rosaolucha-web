<?php
  use Roots\Sage\Helpers;
?>
<section class="l-row">
  <div class="l-inner">

    <?php

      // Exclude distributor pages from works query
      $post__not_in = array(
        get_page_by_path('alaska-y-mario','OBJECT','work')->ID,
        get_page_by_path('galas','OBJECT','work')->ID
      );

      $args = array(
        'post_type'        => 'work', 
        'orderby'          => 'menu_order',
        'order'            => 'asc',
        'posts_per_page'   => -1,
        'post__not_in'     => $post__not_in
      );

      $query = new WP_Query( $args );

    ?>

    <?php if ( $query->have_posts() ) : ?>

    <ul class="works">

    <?php while ( $query->have_posts() ) : $query->the_post(); ?>

      <li class="works-item <?php if ( !empty( get_field('video_url_allworks') ) ) {  echo 'works-item--hastooltip'; } ?>">

        <div class="works-item__thumb"><?= Helpers\get_image('thumbnail'); ?></div><!-- /.work-list-item__thumb -->

        <div class="works-item__content">

          <p class="meta"><strong><?php the_field('year'); ?></strong> - <span><?php the_field('channel'); ?></span></p>
  
          <p class="title">
          <?php if ($post->post_parent) : ?>
          <span><?php echo get_the_title($post->post_parent); ?>:</span>
          <?php endif; ?>
          <span><?php the_title(); ?></span>
          </p>

          <p class="description"><?php the_field('description'); ?></p>
          
          <?php if ( get_field('selected_work') ) : ?>
          <p class="link"><a href="<?php echo get_the_permalink(); ?>"><?php _e('Selected work','rosaolucha'); ?></a></p>
          <?php endif; ?>
        
        </div><!-- /.works-item__content -->

        <?php if ( get_field('video_url_allworks') ) : ?>
        <div class="works-item__tooltip" aria-hidden="true">
          <div class="video">
            <div class="video__wrap"><?= Helpers\embed_video(get_field('video_url_allworks')); ?></div>
          </div>
        </div><!-- /.works-item__tooltip -->
        <?php endif; ?>


      </li><!-- /.works-item -->
  
    <?php endwhile; wp_reset_postdata(); ?>

    </ul><!-- /.work-list -->

    <?php endif; ?>
    

  </div><!-- /.l-inner -->
</section><!-- /.l-row -->