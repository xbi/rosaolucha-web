<section class="prefooter">
  <div class="container">

    <?php
      if (has_nav_menu('follow_menu_footer')) :
        echo '<div class="prefooter__follow nav-contact">';
        echo '  <p class="nav-contact__title">' . __('Follow','rosaolucha') . '</p>';
        wp_nav_menu([
          'theme_location' => 'follow_menu_footer',
          'menu_class' => 'nav-contact__menu',
          'walker' => new social_icons_walker()
        ]);
        echo '</div>';
      endif;
    ?>   

    <?php
      if (has_nav_menu('contact_menu_footer')) :
        echo '<div class="prefooter__contact nav-contact">';
        echo '  <p class="nav-contact__title">' . __('Contacto','rosaolucha') . '</p>';
        wp_nav_menu(['theme_location' => 'contact_menu_footer', 'menu_class' => 'nav-contact__menu']);
        echo '</div>';
      endif;
    ?>   
    
  </div>
</section>
<!-- /.prefooter -->