<?php
  use Roots\Sage\Breadcrumbs;
  use Roots\Sage\Helpers;
  use Roots\Sage\Titles;
  use Roots\Sage\Works;
?>
<section class="hero l-section">

  <header class="hero__header l-row">
    <div class="l-inner">
      <?= Breadcrumbs\breadcrumbs(); ?>
      <?= Works\meta(); ?>
      <div class="page-title">
        <h1 class="page-title__text"><?= Titles\title(); ?></h1>
        <?php if ( get_field('channel_logo') || get_field('channel_logo_2') ): ?>
          <div class="page-title__logos">
            <?php if ( get_field('channel_logo') ): ?><?= Helpers\get_image('channel_logo'); ?><?php endif; ?>
            <?php if ( get_field('channel_logo_2') ): ?><?= Helpers\get_image('channel_logo_2'); ?><?php endif; ?>
          </div><!-- /.page-title__logos -->        
        <?php endif; ?>
      </div><!-- /.page-title -->      
    </div>
  </header><!-- /.hero__header -->

  <?php if ( ( 'work' === get_post_type() ) || is_page( array('bio') ) ) :?>
  <div class="hero__content l-row">
    <div class="l-inner">
      
      <?php

        // Case: Bio
        if ( is_page( array('bio') ) ) get_template_part('templates/bio/content', 'hero');

        // Case: Selected works
        if ( ( 'work' === get_post_type() ) && is_page_template( 'template-distribuidora.php' ) ) get_template_part('templates/work/hero', 'works');

        // Case: "Work" single post
        if ( ( 'work' === get_post_type() ) && !is_page_template( 'template-distribuidora.php' ) ) get_template_part('templates/work/hero', 'video');

      ?>

    </div>
  </div><!-- /.hero__content -->
  <?php endif; ?>

</section>
<!-- /.hero -->

