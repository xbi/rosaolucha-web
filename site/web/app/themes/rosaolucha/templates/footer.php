<footer class="content-info" role="contentinfo">
  <div class="container">
    <a class="brand" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>"><svg class="logo logo--compact" aria-hidden="true"><use xlink:href="#logo-compact"></use></svg></a>  
    <nav class="nav-primary" role="navigation">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
      endif;
      ?>
    </nav>
  </div>
</footer>
