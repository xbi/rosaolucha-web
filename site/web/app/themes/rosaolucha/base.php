<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class('site'); ?>>
    <div class="canvas" id="canvas">
      <?php
        do_action('get_header');
        get_template_part('templates/header');
      ?>
      <div class="wrap container" role="document">
        <div class="content row">
          <main role="main" class="main">
            <?php include Wrapper\template_path(); ?>
          </main><!-- /.main -->
        </div><!-- /.content -->
      </div><!-- /.wrap -->
      <?php
    
        do_action('get_footer');

        if ( !is_page_template( 'template-distribuidora.php' ) ) {
          get_template_part('templates/prefooter');
        }
        
        get_template_part('templates/footer');
    
      ?>
    </div>
    <!-- /.canvas -->
    <?php
      get_template_part('templates/content','svg');
      wp_footer();  
    ?>
  </body>
</html>
