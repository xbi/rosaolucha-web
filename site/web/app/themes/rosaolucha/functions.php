<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',           // Scripts and stylesheets
  'lib/extras.php',           // Custom functions
  'lib/setup.php',            // Theme setup
  'lib/titles.php',           // Page titles
  'lib/wrapper.php',          // Theme wrapper class
  'lib/customizer.php',       // Theme customizer
  'lib/tags/breadcrumbs.php', // Page breadcrumbs
  'lib/tags/helpers.php',     // Theme helpers 
  'lib/tags/works.php'        // Works tags  
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}

unset($file, $filepath);

/*
 * Custom Social Icons Menu Walker
 */
class social_icons_walker extends Walker_Nav_Menu {

  function start_el(&$output, $item, $depth=0, $args=array(), $id=0) {

    $object    = $item->object;
    $type      = $item->type;
    $title     = $item->title;
    $permalink = $item->url;
    $slug      = strtolower($title);

    $output .= '<li class="menu-item">';

    if ( $permalink && $permalink != '#' ) {
      $output .= '<a href="' . $permalink . '">';
    } else {
      $output .= '<span>';
    }

    if ( $slug == 'vimeo' || $slug == 'instagram' ) {
      $output .= '<svg class="icon icon-' . $slug . '" aria-hidden="true"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-' . $slug . '"></use></svg>';
    }

    $output .= '<span>' . $title . '</span>';

    if ( $permalink && $permalink != '#' ) {
      $output .= '</a>';
    } else {
      $output .= '</span>';
    }

  }

}

/*
 * Modify TinyMCE editor to remove undesired tags.
 */

function myplugin_tinymce_buttons( $buttons ) {
  //Add style selector to the beginning of the toolbar
  array_unshift( $buttons, 'styleselect' );
  return $buttons;
 }

add_filter( 'mce_buttons_2', 'myplugin_tinymce_buttons' );

function custom_tinymce_settings($settings) {

  $style_formats = array(  
    array(  
      'title'    => 'Accent',  
      'selector' => 'h3,h4,h5,h6',  
      'classes'  => 'accent'
    ),  
    array(  
      'title'    => 'Big',  
      'selector' => 'p',  
      'classes'  => 'big'
    ),
    array(  
      'title'    => 'Loud',  
      'selector' => 'p',  
      'classes'  => 'loud'
    )
  );  

  $settings['style_formats'] = json_encode( $style_formats ); 

  // Add block format elements you want to show in dropdown
  $settings['block_formats'] = 'Paragraph=p;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6';

  return $settings;

}

add_filter('tiny_mce_before_init', 'custom_tinymce_settings' );

/**
 * Registers an editor stylesheet for the theme.
 */
function wpdocs_theme_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );

