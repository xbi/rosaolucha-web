<?php

namespace Roots\Sage\Breadcrumbs;

/**
 * Page Breadcrumbs
 */
function breadcrumbs() {

  $showCurrent  = 1;                                  // 1 - show current post/page title in breadcrumbs, 0 - don't show 
  $showOnHome   = 0;                                  // 1 - show breadcrumbs on the homepage, 0 - don't show
  $home         = __( 'Inicio', 'rosaolucha' );       // text for the 'Home' link
  $selectedWork = __( 'Selected work', 'rosaolucha'); // text for the 'Selected work' breadcrumb item

  global $post;
  $homeLink = get_bloginfo('url');
 
  if ( is_home() || is_front_page() ) {
 
    if ($showOnHome == 1) echo '<ol class="breadcrumbs" itemtype="http://schema.org/BreadcrumbList"><li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><span itemprop="item" class="current"><span itemprop="name">' . $home . '</span></span><meta itemprop="position" content="1" /></li></ol><!-- /.breadcrumbs -->';
 
  } else {
 
    echo '<ol class="breadcrumbs" itemtype="http://schema.org/BreadcrumbList">';
    echo '<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . $homeLink . '"><span itemprop="name">' . $home . '</span></a><meta itemprop="position" content="1" /></li>';

    if ( is_page() && !$post->post_parent && ( $showCurrent === 1 ) ) {

      echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item" class="current"><span itemprop="name">' . get_the_title() . '</span></span><meta itemprop="position" content="2" /></li>';

    } elseif ( is_single() && ( 'work' === get_post_type() ) ) {

      echo '<li itemprop="itemListElement" itemscope
        itemtype="http://schema.org/ListItem"><span itemprop="item"><span itemprop="name">' . $selectedWork . '</span></span><meta itemprop="position" content="2" /></li>';

      if ( !$post->post_parent && ( $showCurrent === 1 ) ) {
        echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item" class="current"><span itemprop="name">' . get_the_title() . '</span></span><meta itemprop="position" content="3" /></li>';
      }

      if ( $post->post_parent ) {
        
        $parent_id  = $post->post_parent;
        $offset = 3;

        $breadcrumbs = array();

        while ($parent_id) {
          $page = get_post($parent_id);
          $breadcrumbs[] = '<a itemprop="item" href="' . get_permalink($page->ID) . '"><span itemprop="name">' . get_the_title($page->ID) . '</span></a>';
          $parent_id  = $page->post_parent;
        }
        
        $breadcrumbs = array_reverse($breadcrumbs);
        
        for ($i = 0; $i < count($breadcrumbs); $i++) {
          echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . $breadcrumbs[$i] . '<meta itemprop="position" content="' . ($i + $offset) . '" /></li>';
        }
        
        if ($showCurrent === 1) {
          echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item" class="current"><span itemprop="name">' . get_the_title() . '</span></span><meta itemprop="position" content="' . ($i + $offset) . '" /></li>';          
        }
      
      }


    }

    echo '</ol><!-- /.breadcrumbs -->';

  }
} 
