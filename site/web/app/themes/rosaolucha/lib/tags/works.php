<?php

namespace Roots\Sage\Works;

/**
 * Work metadata
 */
function meta() {

  if ( 'work' === get_post_type() ) {
    echo '<div class="work-meta">';
    echo '  <span class="work-meta__item work-meta__item--author">' . __('Rosa Olucha', 'rosaolucha') . '</span>';
    if ( get_field('descripcion_alt') ) {
      echo '  <span class="work-meta__item work-meta__item--description">' . get_field('descripcion_alt') . '</span>';
    } else {
      echo '  <span class="work-meta__item work-meta__item--description">' . get_field('description') . '</span>';      
    }
    echo '</div><!-- /.work-meta -->';
  }

}

/**
 * Work nav
 */
function nav() {

  global $post;

  $parent = wp_get_post_parent_id( $post->ID );

  $args = array(
    'post_parent'      => $parent,
    'post_type'        => 'work', 
    'orderby'          => 'title',
    'order'            => 'asc',
    'posts_per_page'   => -1,
  );

  $pagelist = get_children($args);
  
  $pages = array();
  foreach ($pagelist as $page) {
     $pages[] += $page->ID;
  }
  
  $current = array_search(get_the_ID(), $pages);
  $prevID = ( isset($pages[$current-1]) ) ? $pages[$current-1] : end($pages);
  $nextID = ( isset($pages[$current+1]) ) ? $pages[$current+1] : reset($pages);

  
  if (!empty($prevID)) {
    echo '<a class="work-footer__nav work-footer__nav--prev" href="';
    echo get_permalink($prevID);
    echo '"';
    echo 'title="';
    echo get_the_title($prevID); 
    echo '">';
    echo '<svg class="icon icon-arrow-left"><use xlink:href="#arrow-left"></use></svg><span>';
    echo get_the_title($prevID);
    echo '</span></a>';
  }

  if (!empty($nextID)) {
    echo '<a class="work-footer__nav work-footer__nav--next" href="';
    echo get_permalink($nextID);
    echo '"';
    echo 'title="';
    echo get_the_title($nextID); 
    echo '">';
    echo '<span>';
    echo get_the_title($nextID);
    echo '</span><svg class="icon icon-arrow-right"><use xlink:href="#arrow-right"></use></svg></a>'; 
  }


}