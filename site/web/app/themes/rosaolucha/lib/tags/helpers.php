<?php

namespace Roots\Sage\Helpers;

/**
 * Get ACF image field
 */
function get_image($image_name) {

  $image = get_field($image_name) ? get_field($image_name) : get_sub_field($image_name);

  if ( !empty($image) ) {

    $image_class = '';
    $url         = $image['url'];
    $alt         = $image['alt'];
    $width       = $image['width'];
    $height      = $image['height'];
    $image_ratio = $width / $height;

    // $image_class .= ($image_ratio > 2) ? 'image-ratio--16-9' : ($image_ratio < 1.5) ? 'image-ratio--1-1' : 'image-ratio--4-3'; 

    if ($image_ratio > 2.1) {
      $image_class .= 'image-ratio--16-9';
    } elseif ($image_ratio < 1.3) {
      $image_class .= 'image-ratio--1-1';
    } else {
      $image_class .= 'image-ratio--4-3';
    }

    echo '<img src="' . $url . '" alt="'. $alt .'" class="' . $image_class . '" width="' . $width . '" height="' . $height . '">';

  }

}

/**
 * Embed VIMEO video
 */
function embed_video($vimeoUrl) {

    $fetchVimeoIdArr = explode('/', $vimeoUrl);
    $idCounter = count($fetchVimeoIdArr) - 1;
    $vimeoId = $fetchVimeoIdArr[$idCounter];

    echo '<iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" id="vimeo_player_' . $vimeoId . '" src="https://player.vimeo.com/video/' . $vimeoId . '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';

}


