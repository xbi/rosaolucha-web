/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function ($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {

      init: function () {

        // JavaScript to be fired on all pages
        $('html').removeClass('no-js').addClass('js');

        // Toggle Mobile Menu Behavior
        // --------------------------------------------------------------------
        $('[data-toggle="menu"]').on('click', function () {
          $('html').toggleClass('menu-is-on');
        });

        var resetAllVideos = function () {

          var players = [];

          $.each($('iframe[src*="vimeo.com"]', '.works'), function (i, vimeoIframe) {
            players[i] = new Vimeo.Player(vimeoIframe);
            players[i].unload();
          });

        };

        $(document).keyup(function (event) {

          if (event.keyCode === 27) {

            $('html').removeClass('menu-is-on'); // Hide mobile menu on hitting ESC
            $('.works-item').removeClass('has-over');

            resetAllVideos();

          }

        });

        $(document).on('click', function (event) {

          var target = event.target; //target div recorded

          if (!$(target).is('.works-item__tooltip')) {
            $('.works-item').removeClass('has-over');
            resetAllVideos();
          }

        });


      },
      finalize: function () {

        // Init Flexsliders
        // --------------------------------------------------------------------
        var $sliders = $('.flexslider');

        if ($sliders.length) {

          $.each($sliders, function (i, slider) {

            var $slider = $(slider);
            var isEndorsementsSlider = $slider.hasClass('endorsements-slider');
            var isHighlightsSlider = $slider.hasClass('highlights-slider');
            var isReviewsSlider = $slider.hasClass('reviews-slider');
            var isVideohighlightsSlider = $slider.hasClass('videohighlights-slider');

            // Helper function to format slide counter numbers
            var pad = function (str, max) {
              str = str.toString();
              return str.length < max ? pad("0" + str, max) : str;
            };

            // Init VIMEO players in slides
            if (isVideohighlightsSlider) {

              var $vimeoIframes = $('.flexslider').find('iframe');
              var players = [];

              $.each($vimeoIframes, function (i, vimeoIframe) {
                players[i] = new Vimeo.Player(vimeoIframe);
              });

            }

            var settings = {
              animation: 'slide',
              slideshow: false,
              pauseOnHover: true,
              controlNav: (isEndorsementsSlider || isReviewsSlider || isHighlightsSlider) ? true : false,
              directionNav: (isReviewsSlider || isVideohighlightsSlider) ? true : false,
              smoothHeight: (isReviewsSlider || isVideohighlightsSlider) ? true : false,
              video: (isVideohighlightsSlider) ? true : false,
              allowOneSlide: false,
              start: function (slider) {

                if ($slider.hasClass('videohighlights-slider')) {

                  if (slider.count === 1) {
                    $('.flex-direction-nav', $slider).hide();
                  } // Hide slider nav if there's only 1 slide

                  var current = slider.currentSlide + 1;
                  $slider.find('.flex-direction-nav').append('<li class="flex-counter"><strong id="flex-current">' + pad(current, 2) + '</strong>/<span id="flex-total">' + pad(slider.count, 2) + '</span></li>');

                }

              },
              after: function (slider) {

                if ($slider.hasClass('videohighlights-slider')) {
                  var current = slider.currentSlide + 1;
                  $('#flex-current', $slider).html(pad(current, 2));
                }

              },
              before: function (slider) {
                if (slider.slides.eq(slider.currentSlide).find('iframe').length !== 0) {
                  players[slider.currentSlide].pause();
                }
              }
            };

            $slider.flexslider(settings);

          });

        }

        // Init Videos
        // --------------------------------------------------------------------
        var $videos = $('.video:not(.video-promo)');


        if ($videos.length) {

          $.each($videos, function (i, video) {

            var $video = $(video);
            var $play = $video.find('.icon-play');
            var $player = $video.find('iframe');
            var $wrap = $video.data('wrapper') ? $($video.data('wrapper')) : $video;

            var vimeoPlayer = new Vimeo.Player($player);

            var finishActions = function () {
              setTimeout(function () {
                $video.removeClass('is-playing');
                $wrap.removeClass('has-video-playing');
              }, 2000);
            };

            $play.on('click', function (event) {
              event.preventDefault();
              event.stopPropagation();
              vimeoPlayer.play();
            });

            vimeoPlayer.on('play', function (data) {
              $video.addClass('is-playing');
              $wrap.addClass('has-video-playing');
            });

            vimeoPlayer.on('ended', function (data) {
              finishActions();
            });

            vimeoPlayer.on('timeupdate', function (data) { // Workaround for video player not triggerint "ended" event on some browsers
              if (data.percent >= 0.99) {
                finishActions();
              }
            });

          });

        }


      }
    },
    'all_work': {
      init: function () {

        var $workItemsWithTooltip = $('.works-item--hastooltip');
        var players = [];

        $.each($workItemsWithTooltip, function (i, workItem) {

          var $workItem = $(workItem);
          var $hotspot = $workItem.find('img');
          var $player = $workItem.find('iframe');
          var $vimeoPlayer = new Vimeo.Player($player);

          players[i] = $vimeoPlayer;

          $hotspot.on('mouseenter', function () {

            var $currentItem = $('.works-item--hastooltip').filter('.has-over');
            $currentItem.removeClass('has-over');
            $workItem.addClass('has-over');
            $vimeoPlayer.play().then(function () {
              console.log('Playing', $player.attr('src'));
            }).catch();

          });

        });

      }
    },
    'bio': {
      finalize: function () {

        var $endorsements = $('.endorsement', '.bio .endorsements');

        if ($endorsements.length) {

          $.each($endorsements, function (index, endorsement) {

            $endorsement = $(endorsement);

            $endorsement.on('click', function (e) {

              e.preventDefault();
              var $this = $(this);
              var $over = $this.find('.endorsement__over');

              if ($this.hasClass('active')) {

                $over.slideUp('200', function () {
                  $this.removeClass('active');
                });

              } else {

                var $active = $endorsements.filter('.active');
                var $activeOver = $active.find('.endorsement__over');

                if ($active.length) {

                  $activeOver.slideUp('200', function () {
                    $active.removeClass('active');
                    $over.slideDown('200', function () {
                      $this.addClass('active');
                    });
                  });

                } else {

                  $over.slideDown('200', function () {
                    $this.addClass('active');
                  });

                }

              }

            });

          });

        }

      }
    },
    'single_work': {

      finalize: function () {

        // Clone video cover for FX
        // --------------------------------------------------------------------
        var $heroVideoCover = $('img', '.hero-video');

        if ($heroVideoCover.length) {

          var $target = $heroVideoCover.closest('.hero-video__player');

          $heroVideoCover
            .clone().addClass('cloned').appendTo($target).end()
            .clone().addClass('cloned').appendTo($target);

        }

      }

    }

  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function (func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function () {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
