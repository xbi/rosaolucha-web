<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/home/homepage', 'header'); ?>
  <?php get_template_part('templates/home/homepage', 'works'); ?>
  <?php get_template_part('templates/common/working-for'); ?>    
  <?php get_template_part('templates/home/homepage', 'endorsements'); ?>    
<?php endwhile; ?>
