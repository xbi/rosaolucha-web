## DATOS DEL TRABAJO

- Título (WP title)
- Selected work? (true/false) **selected_work**
- Descripción (text) _por ejemplo: Dirección & guión, Dirección, Realización - 2 temporadas 2014-2015…_ **description**
- Año (text) _por ejemplo: 2014_ **year**
- Canal (text) _por ejemplo: MTV, PARAMOUNT, TELE5…_ **channel**
- Logo del canal (imagen) _logo del canal para ficha del trabajo, tamaño 160x100_ _Condicional: IF selected work = true_ **channel_logo**
- Thumbnail (imagen) _imagen pequeña para "All work", tamaño 320x200_ **thumbnail**
- Imagen destacada (WP featured image)
- Tagline 1 (Título vídeo) _Condicional: IF selected work = true_ **tagline_1**
- Tagline 2 (Subtítulo vídeo) _Condicional: IF selected work = true_ **tagline_2**
- Tagline 3 (Frase homepage) _Condicional: IF selected work = true_ **tagline_3**
- Caratula video (imagen) _Condicional: IF selected work = true_ **video_cover**
- URL video (texto) _Condicional: IF selected work = true_ **video_url**

## CONTENIDO PRINCIPAL

- Contenidos (repeater) **contents**
    + Tipo de contenido [Texto,Imagen,Vídeo Promo] **content_type**
    + Posición [Left,Right] **content_position**
    + Texto (richtext) _Condicional: IF tipo de contenido = Texto_ **content_text**
    + Imagen (imagen) _Condicional: IF tipo de contenido = Imagen_ **content_image**
    + Estilo imagen [pushleft,center,pushright,border] _(Condiciónal: IF tipo de contenido = Imagen)_ **content_image_style**
    + Caratula video promo (imagen) _Condicional: IF tipo de contenido = video_ **content_video_cover** 
    + URL video promo (text) _Condicional: IF tipo de contenido = video_ **content_video_url**

## SLIDER VIDEO-HIGHLIGHTS

- Slides (repeater) **videohighlights**
    + Texto slide (text) **videohighlight_text**
    + Carátula video (imagen) **videohighlight_cover**
    + URL video (text) **videohighlight_url**

## SLIDER HIGHLIGHTS

- Logo (imagen) **highlights_logo**
- Slides (repeater) **highlights**
    + Título slide (text) **highlight_title**
    + Texto slide (text) **highlight_text**

## RATINGS

- Texto (richtext) **ratings_text**
- Tabla 1 (richtext) **ratings_table_1**
- Tabla 2 (richtext) **ratings_table_2**

## QUE SE DIJO

- Título sección (texto) **reviews_section_title**
- Título del slider (texto) **reviews_title**
- Fotos (repeater) **reviews_images**
    + Foto (imagen) **reviews_image**
    + Posición foto (texto) [left,right,bottom] **reviews_image_position**
- Críticas (repeater) **reviews**
    + Texto crítica (texto) **review_text**
    + PDF (link to doc) **review_pdf**
    + Logo del medio (imagen) **review_logo**

## PREMIOS

- Premios (repeater) **awards**
    + Título (text) **award_title**
    + Texto (text) **award_text**

## THEME _Ubicar en sidebar_ **color_theme**

- Color theme [verde,negro,azul,rosa,amarillo]