### Reference:

- https://code.lengstorf.com/learn-trellis-wordpress-roots/
- https://sprintworks.se/blog/trellis
- https://discourse.roots.io/t/adding-svg-support-to-the-asset-pipeline/3883
- https://managewp.com/custom-post-types-pt-1
- http://www.degordian.com/education/blog/best-practice-for-responsive-typography/

- http://www.wpbeginner.com/wp-tutorials/how-to-display-a-list-of-child-pages-for-a-parent-page-in-wordpress/
- http://www.techpulsetoday.com/wordpress-breadcrumbs-without-plugin/
- http://ianrmedia.unl.edu/responsive-table-generator-tool
- https://www.theportlandcompany.com/2018/04/30/absolute-beginners-guide-to-roots-trellis-bedrock-sage/
- https://theblogpros.com/2019/02/28/i-spun-up-a-scalable-wordpress-server-environment-with-trellis-and-you-can-too/


### Update project to Ansible 2.4.0

- https://github.com/roots/trellis/commit/d96a58f4e2c31b261c7b03cdf2341d54dd1c6110
- https://discourse.roots.io/t/updated-to-ansible-2-4-deploys-broken-now-what/10588/5
